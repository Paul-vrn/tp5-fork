const Sequelize = require("sequelize");
const DataTypes = Sequelize.DataTypes;

module.exports = db => {
  const bookmarks = db.define(
    "bookmarks",
    {
      id: {
        primaryKey: true,
        type: DataTypes.INTEGER,
        autoIncrement: true,
      },
      title: {
        type: DataTypes.STRING(128),
        allowNull: false,
        unique: true,
      },
      description: {
        type: DataTypes.TEXT("medium"),
      },
      link: {
        type: DataTypes.STRING(2083),
      },
    },
    { timestamps: false }
  );
  return bookmarks;
};
