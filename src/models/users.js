const Sequelize = require("sequelize");
const DataTypes = Sequelize.DataTypes;
module.exports = db => {
  const users = db.define(
    "users",
    {
      id: {
        primaryKey: true,
        type: DataTypes.INTEGER,
        autoIncrement: true,
      },
      username: {
        type: DataTypes.STRING(16),
        allowNull: false,
        unique: true,
      },
    },
    { timestamps: false }
  );
  return users;
};
