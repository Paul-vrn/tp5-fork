const Sequelize = require("sequelize");
const DataTypes = Sequelize.DataTypes;

module.exports = db => {
  const tags = db.define(
    "tags",
    {
      id: {
        primaryKey: true,
        type: DataTypes.INTEGER,
        autoIncrement: true,
      },
      name: {
        type: DataTypes.STRING(128),
        allowNull: false,
      },
    },
    { timestamps: false }
  );
  return tags;
};
