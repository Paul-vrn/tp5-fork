// Load Enviroment Variables to process.env (if not present take variables defined in .env file)
require("mandatoryenv").load(["DATABASE", "DATABASE_DIALECT"]);
const { DATABASE, DATABASE_DIALECT } = process.env;

const Sequelize = require("sequelize");
let sequelize;

if (DATABASE_DIALECT === "sqlite") {
  sequelize = new Sequelize({
    storage: DATABASE,
    dialect: DATABASE_DIALECT,
    logging: false,
  });
} else {
  // database pour saclingo avec postgres
  sequelize = new Sequelize(DATABASE, {
    dialect: DATABASE_DIALECT,
    ssl: {
      rejectUnauthorized: false,
      require: true,
    },
    logging: false,
  });
}
// On importe les modèles
const userModel = require("./users.js")(sequelize);
const tagModel = require("./tags.js")(sequelize);
const bookmarkModel = require("./bookmarks.js")(sequelize);
// On définit les relations
userModel.hasMany(tagModel);
userModel.hasMany(bookmarkModel);
tagModel.belongsToMany(bookmarkModel, { through: "bookmark_tag" });
bookmarkModel.belongsToMany(tagModel, { through: "bookmark_tag" });
// On exporte le tout dans un objet
const db = {
  userModel,
  tagModel,
  bookmarkModel,
  sequelize,
  Sequelize,
};

module.exports = db;
