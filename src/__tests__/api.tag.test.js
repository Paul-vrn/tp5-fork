/* eslint-env jest */
const app = require("../app");
const request = require("supertest");

let auth = null;
beforeAll(async () => {
  auth = await request(app).get("/getjwtDeleg/paul");
});

describe("GET /bmt/paul/tags", () => {
  test("Test if get tags works with initialized table tag", async () => {
    const response = await request(app)
      .get("/bmt/paul/tags")
      .set("x-access-token", auth.body.token);
    expect(response.statusCode).toBe(200);
    expect(response.body.message).toBe("Returning tags");
    expect(response.body.data.length).toBe(0);
  });
});

describe("Scénario ajout -> modification -> suppression d'un tag", () => {
  let tagId = null;
  describe("POST /bmt/paul/tags", () => {
    test("Test if post tag works", async () => {
      const response = await request(app)
        .post("/bmt/paul/tags")
        .set("x-access-token", auth.body.token)
        .set("Content-Type", "application/json")
        .send({ name: "tag1" });
      expect(response.statusCode).toBe(201);
      expect(response.body.message).toBe("Tag Added");
      expect(response.body.data.name).toBe("tag1");
      tagId = response.body.data.id;
    });
  });

  describe(`PUT /bmt/paul/tags/${tagId}`, () => {
    test("Test if put tag works", async () => {
      const response = await request(app)
        .put(`/bmt/paul/tags/${tagId}`)
        .set("x-access-token", auth.body.token)
        .set("Content-Type", "application/json")
        .send({ name: "tag2" });
      expect(response.statusCode).toBe(200);
      expect(response.body.message).toBe("1 tag updated");
    });
  });

  describe(`DELETE /bmt/paul/tags/${tagId}`, () => {
    test("Test if delete tag works", async () => {
      const response = await request(app)
        .delete(`/bmt/paul/tags/${tagId}`)
        .set("x-access-token", auth.body.token);
      expect(response.statusCode).toBe(200);
      expect(response.body.message).toBe("Tag deleted");
    });
  });
});
