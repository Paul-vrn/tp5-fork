/* eslint-env jest */
const app = require("../app");
const request = require("supertest");

let auth = null;
beforeAll(async () => {
  auth = await request(app).get("/getjwtDeleg/paul");
});

describe("GET /api/users", () => {
  test("Test if get users works with initialized table user", async () => {
    const response = await request(app).get("/bmt/users").set("x-access-token", auth.body.token);
    expect(response.statusCode).toBe(200);
    expect(response.body.message).toBe("Returning users");
    expect(response.body.data.length).toBe(1);
  });
});

describe("POST /api/users", () => {
  test("Test if post user works", async () => {
    const response = await request(app)
      .post("/bmt/users")
      .set("x-access-token", auth.body.token)
      .set("Content-Type", "application/json")
      .send({ username: "john" });
    expect(response.statusCode).toBe(201);
    expect(response.body.message).toBe("User Added");
    expect(response.body.data.username).toBe("john");
  });
});
