const swaggerAutogen = require("swagger-autogen")({ language: "fr", openapi: "3.0.0" });

require("mandatoryenv").load(["BASE_URL", "PROTOCOL"]);
const { BASE_URL, PROTOCOL } = process.env;
const outputFile = "swagger_output.json";
const endpointsFiles = ["src/routes/router.js"];

const addUser = {
  username: "john",
};
const addBookmark = {
  title: "My bookmark",
  description: "My bookmark description",
  link: "https://www.google.com",
  tags: [
    {
      $ref: "#/definitions/Tag",
    },
  ],
};
const addTag = {
  name: "My tag",
};

const doc = {
  info: {
    version: "1.0.0",
    title: "Bookmark API",
    description: "API for bookmarks",
  },
  securityDefinitions: {
    apiKeyHeader: {
      name: "x-access-token",
      type: "apiKey",
      in: "header",
      description: "Token",
    },
  },
  host: BASE_URL, // change en fonction de l'environnement (local : localhost:3000, prod : scalingo.io)
  basePath: "/",
  schemes: [PROTOCOL], // en local http et en prod https (scalingo accepte que https)
  consumes: ["application/json"],
  produces: ["application/json"],
  tags: [
    {
      name: "Auth",
      description: "API for auth in the system",
    },
    {
      name: "Users",
      description: "API for users in the system",
    },
    {
      name: "Bookmarks",
      description: "API for bookmarks in the system",
    },
    {
      name: "Tags",
      description: "API for tags in the system",
    },
  ],
  definitions: {
    User: { id: 1, ...addUser },
    Bookmark: { id: 1, ...addBookmark },
    Tag: { id: 1, ...addTag },
    addUser: addUser,
    addBookmark: addBookmark,
    addTag: addTag,
  },
};

swaggerAutogen(outputFile, endpointsFiles, doc);
