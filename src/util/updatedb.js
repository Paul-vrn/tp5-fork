// eslint-disable-next-line no-unexpected-multiline
(async () => {
  // Regénère la base de données
  const db = require("../models/database.js");
  await db.sequelize.sync({ force: true });
  console.log("Base de données créée.");
  // Initialise la base avec quelques données
  await db.userModel.create({
    username: "paul",
  });
})();
