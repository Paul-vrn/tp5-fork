const express = require("express");
// mergeParams: true est nécessaire pour que les paramètres de la route parente soient disponibles dans les routes enfants
const router = express.Router({ mergeParams: true });
const tag = require("../controllers/tag.js");

router.get("/", tag.getTags);
router.get("/:tid", tag.getTag);
router.post("/", tag.newTag);
router.put("/:tid", tag.putTag);
router.delete("/:tid", tag.deleteTag);

module.exports = router;
