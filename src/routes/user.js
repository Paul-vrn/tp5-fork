const express = require("express");
const router = express.Router();
const user = require("../controllers/user.js");

router.get("/", user.getUsers);
router.post("/", user.newUser);
router.delete("/", user.deleteUsers);

module.exports = router;
