const express = require("express");
// mergeParams: true est nécessaire pour que les paramètres de la route parente soient disponibles dans les routes enfants
const router = express.Router({ mergeParams: true });
const bookmark = require("../controllers/bookmark.js");

router.get("/", bookmark.getBookmarks);
router.get("/:tid", bookmark.getBookmark);
router.post("/", bookmark.newBookmark);
router.put("/:tid", bookmark.putBookmark);
router.delete("/:tid", bookmark.deleteBookmark);

module.exports = router;
