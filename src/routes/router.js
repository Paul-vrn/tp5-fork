const express = require("express");
const router = express.Router();
const checkTokenMiddleware = require("../controllers/middleware.js").checkTokenMiddleware;
const user = require("../controllers/user.js");

router.use("/bmt/users", checkTokenMiddleware, require("./user")); // on définit une route spécifique pour /bmt/users
router.use("/bmt/:user/tags", checkTokenMiddleware, require("./tags")); // on définit une route spécifique pour /bmt/:user/tags
router.use("/bmt/:user/bookmarks", checkTokenMiddleware, require("./bookmarks")); // on définit une route spécifique pour /bmt/:user/bookmarks
// le reste des routes ne nécessite pas de token
router.get("/getjwtDeleg/:user", user.getJwtDeleg);
router.get("/whoami", user.whoami);
router.use("/", (req, res) => {
  // toutes les autres routes renvoient une erreur 405
  res.status(405).send({ message: "Méthode non autorisée" });
});

module.exports = router;
