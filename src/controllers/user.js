const status = require("http-status");
const { userModel } = require("../models/database.js");
const has = require("has-keys");
const CodeError = require("../util/CodeError.js");
const jws = require("jws");
require("mandatoryenv").load(["TOKENSECRET"]);
const { TOKENSECRET } = process.env;

module.exports = {
  async getUsers(req, res) {
    // #swagger.tags = ['Users']
    // #swagger.summary = 'Get All users'
    // #swagger.security = [{'apiKeyHeader': [] }]
    userModel
      .findAll({ attributes: ["id", "username"] })
      .then(data => {
        /* #swagger.responses[200] = {
          description: 'Returning users',
          schema: { $message: 'Returning users', $data: [ { '$ref': '#/definitions/User' } ] },
        } */
        res.json({ message: "Returning users", data }); // par défaut status 200
      })
      .catch(err => {
        /* #swagger.responses[500] = { description: 'Error getting users', schema: { $message: 'Error getting users' } } */
        res.status(status.INTERNAL_SERVER_ERROR).json({ message: "Error getting users", err });
      });
  },
  async newUser(req, res) {
    // #swagger.tags = ['Users']
    // #swagger.summary = 'New User'
    // #swagger.security = [{'apiKeyHeader': [] }]
    // #swagger.parameters['user'] = { in: 'body', description:'Username', schema: { $ref : '#/definitions/addUser' }}
    if (!has(req.body, ["username"]))
      // #swagger.responses[400] = { description: 'You must specify a username', schema: { $message: 'You must specify a username' } }
      throw new CodeError("You must specify a username", status.BAD_REQUEST);
    const { username } = req.body;
    userModel
      .create({ username })
      .then(data => {
        /* #swagger.responses[201] = {
          description: 'User Added',
          schema: { $message: 'User Added', $data: { $ref: '#/definitions/User' }},
        }
        */
        res.status(status.CREATED).json({ message: "User Added", data });
      })
      .catch(() => {
        /* #swagger.responses[500] = { description: 'Error creating user', schema: { $message: 'Error creating user' } } */
        res.status(status.INTERNAL_SERVER_ERROR).json({ message: "Error creating user" });
      });
  },
  async deleteUsers(req, res) {
    // #swagger.tags = ['Users']
    // #swagger.summary = 'Delete User'
    // #swagger.security = [{'apiKeyHeader': [] }]
    userModel
      .destroy({ where: {} })
      .then(data => {
        /* #swagger.responses[200] = { description: 'Users deleted', schema: { $message: 'Users deleted', $data: 1 } } */
        res.json({ message: "Users deleted", data });
      })
      .catch(() => {
        /* #swagger.responses[500] = { description: 'Error deleting users', schema: { $message: 'Error deleting users' } } */
        res.status(status.INTERNAL_SERVER_ERROR).json({ message: "Error deleting users" });
      });
  },
  async getJwtDeleg(req, res) {
    // #swagger.tags = ['Auth']
    // #swagger.summary = 'Get JWT token'
    const user = req.params.user;
    // check if user exists
    userModel
      .findOne({ where: { username: user } })
      .then(user => {
        const token = jws.sign({
          header: { alg: "HS256" },
          payload: user.username,
          secret: TOKENSECRET,
        });
        /* #swagger.responses[200] = {
          description: 'Returning token',
          schema: { message: "Returning token", token: "token" }
        }
        */
        res.json({ message: "Returning token", token });
      })
      .catch(() => {
        // #swagger.responses[403] = { description: 'User not found', schema: { $message: 'User not found' } }
        res.status(status.FORBIDDEN).json({ message: "User not found" });
      });
  },
  async whoami(req, res) {
    // #swagger.path = '/whoami'
    // #swagger.tags = ['Auth']
    // #swagger.summary = 'Get username from token'
    // récupération du token qui est dans x-access-token
    const token = req.headers["x-access-token"];
    // si le token n'est pas présent
    if (!token) {
      return res.status(403).send({ message: "No token provided." });
    }
    // vérification du token
    const verified = jws.verify(token, "HS256", TOKENSECRET);
    if (!verified) {
      return res.status(401).send({ message: "Unauthorized!" });
    }
    const decoded = jws.decode(token);
    // check if user exists
    userModel
      .findOne({ where: { username: decoded.payload } })
      .then(user => {
        // #swagger.responses[200] = { description: 'Returning username', schema: { $data: 'username' } }
        res.json({ data: user.username });
      })
      .catch(() => {
        // #swagger.responses[404] = { description: 'Error : user not found', schema: { $message: 'Error : user not found' } }
        res.status(status.NOT_FOUND).json({ message: "Error : user not found" });
      });
  },
};
