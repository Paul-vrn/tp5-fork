const status = require("http-status");
const { tagModel, userModel, bookmarkModel, sequelize } = require("../models/database.js");
const has = require("has-keys");
const CodeError = require("../util/CodeError.js");
require("mandatoryenv").load(["TOKENSECRET"]);

module.exports = {
  async getBookmarks(req, res) {
    // #swagger.tags = ['Bookmarks']
    // #swagger.summary = 'Get All users'
    // #swagger.security = [{'apiKeyHeader': [] }]
    const user = await userModel.findOne({ where: { username: req.params.user } });
    // #swagger.responses[404] = { description: 'User not found', schema: { $message: 'User not found' } }
    if (!user) throw new CodeError("User not found", status.NOT_FOUND);
    user
      .getBookmarks({
        attributes: ["id", "title", "description", "link"], // on veut pas afficher userId
        include: [
          {
            model: tagModel,
            attributes: ["id", "name"],
            through: { attributes: [] }, // on veut pas afficher la table de jointure
          },
        ],
      })
      .then(data => {
        /* #swagger.responses[200] = {
             description: 'Returning bookmarks',
             schema: { $message: 'Returning bookmarks', $data: [ { '$ref': '#/definitions/Bookmark' } ] },
        } */
        res.json({ message: "Returning bookmarks", data });
      })
      .catch(() => {
        // #swagger.responses[500] = { description: 'Error getting bookmarks', schema: { $message: 'Error getting bookmarks' } }
        res.status(status.INTERNAL_SERVER_ERROR).json({ message: "Error getting bookmarks" });
      });
  },
  async newBookmark(req, res) {
    // #swagger.tags = ['Bookmarks']
    // #swagger.summary = 'New Bookmark'
    // #swagger.security = [{'apiKeyHeader': [] }]
    // #swagger.parameters['bookmark'] = { in: 'body', description: 'Bookmark to add', schema: { $ref: '#/definitions/addBookmark' } }
    if (!has(req.body, ["title", "description", "link", "tags"]))
      // #swagger.responses[400] = { description: 'One or many params are missing', schema: { $message: 'One or many params are missing' } }
      throw new CodeError("One or many params are missing", status.BAD_REQUEST);

    const user = await userModel.findOne({ where: { username: req.params.user } });
    // #swagger.responses[404] = { description: 'User not found', schema: { $message: 'User not found' } }
    if (!user) throw new CodeError("User not found", status.NOT_FOUND);
    const { title, description, link, tags } = req.body;
    sequelize
      .transaction(async transaction => {
        const bookmark = await user.createBookmark({ title, description, link }, { transaction });
        const tagss = await tagModel.findAll(
          { where: { id: tags.map(tag => tag.id) } },
          { transaction }
        );
        // #swagger.responses[400] = { description: 'One or many tags are invalid', schema: { $message: 'One or many tags are invalid' } }
        if (tagss.length !== tags.length)
          throw new CodeError("One or many tags are invalid", status.BAD_REQUEST);
        await bookmark.addTags(tagss, { transaction });
      })
      .then(data => {
        // #swagger.responses[201] = { description: 'Bookmark Added', schema: { $message: 'Bookmark Added', $data: { '$ref': '#/definitions/Bookmark' } } }
        res.status(status.CREATED).json({ message: "Bookmark Added", data });
      })
      .catch(err => {
        // le ?? : si err.code est undefined, on prend status.INTERNAL_SERVER_ERROR
        // #swagger.responses[500] = { description: 'Error creating bookmark', schema: { $message: 'Error creating bookmark', $error: 'Error message', $details: 'Error details' } }
        res
          .status(err.code ?? status.INTERNAL_SERVER_ERROR)
          .json({ error: err.message, details: err.errors });
      });
  },
  async getBookmark(req, res) {
    // #swagger.tags = ['Bookmarks']
    // #swagger.summary = 'Get Bookmark'
    // #swagger.security = [{'apiKeyHeader': [] }]
    bookmarkModel
      .findOne({
        where: { id: req.params.tid },
        attributes: ["id", "title", "description", "link"], // on veut pas afficher userId
        include: [
          {
            model: tagModel,
            attributes: ["id", "name"],
            through: { attributes: [] }, // on veut pas afficher la table de jointure
          },
        ],
      })
      .then(data => {
        // #swagger.responses[200] = { description: 'Returning bookmark', schema: { $message: 'Returning bookmark', $data: { '$ref': '#/definitions/Bookmark' } } }
        res.json({ message: "Returning bookmark", data });
      })
      .catch(() => {
        // #swagger.responses[500] = { description: 'Error getting bookmark', schema: { $message: 'Error getting bookmark' } }
        res.status(status.INTERNAL_SERVER_ERROR).json({ message: "Error getting bookmark" });
      });
  },
  async deleteBookmark(req, res) {
    // #swagger.tags = ['Bookmarks']
    // #swagger.summary = 'Delete Bookmark'
    // #swagger.security = [{'apiKeyHeader': [] }]
    const tid = req.params.tid;
    bookmarkModel
      .destroy({ where: { id: tid } })
      .then(data => {
        // #swagger.responses[200] = { description: 'Bookmark deleted', schema: { $message: 'Bookmark deleted' } }
        res.json({ message: `${data[0]} bookmark deleted` });
      })
      .catch(() => {
        // #swagger.responses[500] = { description: 'Error deleting bookmark', schema: { $message: 'Error deleting bookmark' } }
        res.status(status.INTERNAL_SERVER_ERROR).json({ message: "Error deleting tag" });
      });
  },
  async putBookmark(req, res) {
    // #swagger.tags = ['Bookmarks']
    // #swagger.summary = 'Update Bookmark'
    // #swagger.security = [{'apiKeyHeader': [] }]
    // #swagger.parameters['bookmark'] = { in: 'body', description: 'Bookmark to update', schema: { $ref: '#/definitions/addBookmark' } }
    const tid = req.params.tid;
    const { title, description, link, tags } = req.body;

    const bookmark = await bookmarkModel.findByPk(tid);
    // #swagger.responses[404] = { description: 'Bookmark not found', schema: { $message: 'Bookmark not found' } }
    if (!bookmark) throw new CodeError("Bookmark not found", status.NOT_FOUND);

    sequelize
      .transaction(async transaction => {
        const existingTags = await tagModel.findAll(
          { where: { id: tags.map(tag => tag.id) } },
          { transaction }
        );
        if (existingTags.length !== tags.length)
          // #swagger.responses[400] = { description: 'One or many tags are invalid', schema: { $message: 'One or many tags are invalid' } }
          throw new CodeError("One or many tags are invalid", status.BAD_REQUEST);
        const bookmarkTags = await bookmark.getTags({ transaction });

        const tagsToAdd = existingTags.filter(tag => !bookmarkTags.some(t => t.id === tag.id)); // tags de la requete qui ne sont pas déjà dans le bookmark
        const tagsToRemove = bookmarkTags.filter(tag => !existingTags.some(t => t.id === tag.id)); // tags du bookmark qui ne sont pas dans la requete

        await bookmark.update({ title, description, link }, { transaction });
        await bookmark.removeTags(tagsToRemove, { transaction });
        await bookmark.addTags(tagsToAdd, { transaction });
      })
      .then(data => {
        // #swagger.responses[200] = { description: 'Bookmark updated', schema: { $message: 'Bookmark updated', $data: { '$ref': '#/definitions/Bookmark' } } }
        res.json({ message: "Bookmark updated", data });
      })
      .catch(() => {
        // #swagger.responses[500] = { description: 'Error updating bookmark', schema: { $message: 'Error updating bookmark' } }
        res.status(status.INTERNAL_SERVER_ERROR).json({ message: "Error updating bookmark" });
      });
  },
};
