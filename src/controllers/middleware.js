const jws = require("jws");
require("mandatoryenv").load(["TOKENSECRET"]);
const { TOKENSECRET } = process.env;
const status = require("http-status");
const { userModel } = require("../models/database.js");
/* Vérification du token */
const checkTokenMiddleware = (req, res, next) => {
  const token = req.headers["x-access-token"];
  // si le token n'est pas présent
  if (!token) {
    return res.status(403).send({ message: "No token provided." });
  }
  // vérification du token
  const verified = jws.verify(token, "HS256", TOKENSECRET);
  if (!verified) {
    return res.status(401).send({ message: "Unauthorized!" });
  }
  const decoded = jws.decode(token);
  // check if user exists
  userModel
    .findOne({ where: { username: decoded.payload } })
    .then(() => {
      next(); // continue to next middleware
    })
    .catch(() => {
      res.status(status.NOT_FOUND).json({ message: "Error user not found" });
    });
};

module.exports.checkTokenMiddleware = checkTokenMiddleware;
