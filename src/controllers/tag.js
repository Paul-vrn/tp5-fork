const status = require("http-status");
const { userModel, tagModel } = require("../models/database.js");
const has = require("has-keys");
const CodeError = require("../util/CodeError.js");
require("mandatoryenv").load(["TOKENSECRET"]);

module.exports = {
  async getTags(req, res) {
    // #swagger.tags = ['Tags']
    // #swagger.summary = 'Get All tags'
    // #swagger.security = [{'apiKeyHeader': [] }]
    const user = await userModel.findOne({ where: { username: req.params.user } });
    user
      .getTags({ attributes: ["id", "name"] })
      .then(data => {
        /* #swagger.responses[200] = {
          description: 'Returning tags',
          schema: { $message: 'Returning tags', $data: [ { '$ref': '#/definitions/Tag' } ] },
        } */
        res.json({ message: "Returning tags", data });
      })
      .catch(err => {
        /* #swagger.responses[500] = { description: 'Error getting tags', schema: { $message: 'Error getting tags' } } */
        res.status(status.INTERNAL_SERVER_ERROR).json({ message: "Error", err });
      });
  },
  async newTag(req, res) {
    // #swagger.tags = ['Tags']
    // #swagger.summary = 'New Tag'
    // #swagger.security = [{'apiKeyHeader': [] }]
    // #swagger.parameters['tag'] = { in: 'body', description: 'Tag to add', schema: { $ref: '#/definitions/addTag' } }

    // get id from user
    const user = await userModel.findOne({
      where: { username: req.params.user },
      attributes: ["id"],
    });
    if (!has(req.body, ["name"]))
      // #swagger.responses[400] = { description: 'One or many params are missing', schema: { $message: 'One or many params are missing' } }
      throw new CodeError("You must specify a username", status.BAD_REQUEST);
    const { name } = req.body;
    user
      .createTag({ name: name })
      .then(data => {
        // #swagger.responses[201] = { description: 'Tag Added', schema: { $message: 'Tag Added', $data: { '$ref': '#/definitions/Tag' } } }
        res.status(status.CREATED).json({ message: "Tag Added", data });
      })
      .catch(() => {
        // #swagger.responses[500] = { description: 'Error creating tag', schema: { $message: 'Error creating tag' } }
        res.status(status.INTERNAL_SERVER_ERROR).json({ message: "Error creating tag" });
      });
  },
  async getTag(req, res) {
    // #swagger.tags = ['Tags']
    // #swagger.summary = 'Get a tag'
    // #swagger.security = [{'apiKeyHeader': [] }]
    tagModel
      .findOne({
        where: { id: req.params.tid },
        attributes: ["id", "name"],
      })
      .then(data => {
        // #swagger.responses[200] = { description: 'Returning tag', schema: { $message: 'Returning tag', $data: { '$ref': '#/definitions/Tag' } } }
        res.json({ message: "Returning tag", data });
      })
      .catch(() => {
        // #swagger.responses[500] = { description: 'Error : no tag found', schema: { $message: 'Error : no tag found' } }
        res.status(status.INTERNAL_SERVER_ERROR).json({ message: "Error : no tag found" });
      });
  },
  async deleteTag(req, res) {
    // #swagger.tags = ['Tags']
    // #swagger.summary = 'Delete a tag'
    // #swagger.security = [{'apiKeyHeader': [] }]
    const tid = req.params.tid;
    tagModel
      .destroy({ where: { id: tid } })
      .then(data => {
        // #swagger.responses[200] = { description: 'Tag deleted', schema: { $message: 'Tag deleted', $data: { '$ref': '#/definitions/Tag' } } }
        res.json({ message: "Tag deleted", data });
      })
      .catch(() => {
        // #swagger.responses[500] = { description: 'Error deleting tag', schema: { $message: 'Error deleting tag' } }
        res.status(status.INTERNAL_SERVER_ERROR).json({ message: "Error deleting tag" });
      });
  },
  async putTag(req, res) {
    // #swagger.tags = ['Tags']
    // #swagger.summary = 'Update a tag'
    // #swagger.security = [{'apiKeyHeader': [] }]
    // #swagger.parameters['tag'] = { in: 'body', description: 'Tag to update', schema: { $ref: '#/definitions/Tag' } }
    const tid = req.params.tid;
    const { name } = req.body;
    tagModel
      .update({ name }, { where: { id: tid } })
      .then(data => {
        // #swagger.responses[200] = { description: 'Tag updated', schema: { $message: ' X tag updated' } }
        res.json({ message: `${data[0]} tag updated` });
      })
      .catch(() => {
        // #swagger.responses[500] = { description: 'Error updating tag', schema: { $message: 'Error updating tag' } }
        res.status(status.INTERNAL_SERVER_ERROR).json({ message: "Error updating tag" });
      });
  },
};
