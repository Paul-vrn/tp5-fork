/* Base URL of the web-service for the current user and access token */
//const backend = "https://cawrest.osc-fr1.scalingo.io"; // replace by the backend to use
//const backend = "http://localhost:3000"; // replace by the backend to use

const token = "eyJhbGciOiJIUzI1NiJ9.cGF1bA.WSZxS1U5wLMnrR576kXbZfkLpxHfcKFVvUVVeMCCRGg"; //replace by your token : go to BACKEND/getjwsDeleg/caw to obtain it
const wsBase = `${backend}/bmt/paul/`; // replace USER by your login used to obtain TOKEN

/**
 * Object containing all the API calls,
 * exported as a default object.
 */
var API = {
  whoami: () =>
    fetch(`${backend}/whoami`, {
      method: "GET",
      headers: {
        "x-access-token": token,
      },
    }),
  getBookmarks: () =>
    fetch(`${wsBase}bookmarks`, {
      method: "GET",
      headers: {
        "x-access-token": token,
      },
    }),
  addBookmark: data =>
    fetch(`${wsBase}bookmarks`, {
      method: "POST",
      headers: {
        "x-access-token": token,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    }),
  deleteBookmark: id =>
    fetch(`${wsBase}bookmarks/${id}`, {
      method: "DELETE",
      headers: {
        "x-access-token": token,
      },
    }),
  updateBookmark: (id, data) =>
    fetch(`${wsBase}bookmarks/${id}`, {
      method: "PUT", // idéalement ici, on devrait utiliser la méthode PATCH pour ne modifier que les champs modifiés
      // Cependant le serveur n'a pas de endpoint PATCH.
      headers: {
        "x-access-token": token,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    }),
  getTags: () =>
    fetch(`${wsBase}tags`, {
      method: "GET",
      headers: {
        "x-access-token": token,
      },
    }),
  addTag: tagName =>
    fetch(`${wsBase}tags`, {
      method: "POST",
      headers: {
        "x-access-token": token,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ name: tagName }),
    }),
  updateTag: (id, tagName) =>
    fetch(`${wsBase}tags/${id}`, {
      method: "PUT",
      headers: {
        "x-access-token": token,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ name: tagName }),
    }),
  deleteTag: id =>
    fetch(`${wsBase}tags/${id}`, {
      method: "DELETE",
      headers: {
        "x-access-token": token,
      },
    }),
};

// importation de l'objet API

/* Shows the identity of the current user */
function setIdentity() {
  API.whoami()
    .then(response => response.json())
    .then(data => {
      document.querySelector("h1 span").textContent = data.data;
    })
    .catch(error => {
      alert(error);
      throw error;
    });
}

/* Sets the height of <div id="#contents"> to benefit from all the remaining place on the page */
function setContentHeight() {
  let availableHeight = window.innerHeight;
  availableHeight -= document.getElementById("contents").offsetTop;
  availableHeight -= 2 * document.querySelector("h1").offsetTop;
  availableHeight -= 4 * 1;
  document.getElementById("contents").style.height = availableHeight + "px";
}

/* Selects a new object type : either "bookmarks" or "tags" */
function selectObjectType(type) {
  const selected = document.querySelector("#menu li.selected");
  if (selected === null) {
    document.querySelector(`#menu li.${type}`).classList.add("selected");
  } else {
    if (selected.classList.contains(type)) return;
    selected.classList.remove("selected");
    document.querySelector(`#menu li.${type}`).classList.add("selected");
  }
  if (type === "tags") {
    document.querySelector("#add div.tag").classList.add("selected");
    document.querySelector("#add div.bookmark").classList.remove("selected");
    listTags();
  } else {
    document.querySelector("#add div.bookmark").classList.add("selected");
    document.querySelector("#add div.tag").classList.remove("selected");
    listBookmarks();
  }
}

/* Loads the list of all bookmarks and displays them */
function listBookmarks() {
  document.getElementById("items").innerHTML = ""; // remove all child nodes
  API.getBookmarks()
    .then(response => response.json())
    .then(data => {
      data.data.forEach(bookmark => {
        // on clone le template et on le remplit
        const newBookmark = document.querySelector("div.model.bookmark").cloneNode(true);

        newBookmark.querySelector("h2").textContent = bookmark.title;
        newBookmark.querySelector("a").textContent = bookmark.link;
        newBookmark.querySelector("a").setAttribute("href", bookmark.link);
        newBookmark.querySelector("div.description").textContent = bookmark.description;
        newBookmark.setAttribute("num", bookmark.id);
        newBookmark.classList.remove("model");
        newBookmark.classList.add("item");
        bookmark.tags.forEach(tag => {
          const li = document.createElement("li");
          li.textContent = tag.name;
          li.setAttribute("num", tag.id);
          newBookmark.querySelector("ul.tags").appendChild(li);
        });
        document.getElementById("items").appendChild(newBookmark);
      });
    })
    .catch(error => {
      alert(error);
      throw error;
    });
}

function addBookmark() {
  const title = document.querySelector("#add input[name='title']").value;
  const url = document.querySelector("#add input[name='link']").value;
  const description = document.querySelector("#add input[name='description']").value;
  // Pas besoin de vérifier les champs, si ils sont vides, on tombera dans le catch du fetch
  const data = {
    title: title,
    link: url,
    description: description,
    tags: [], // à l'ajout, pas de tags, on les ajoute après (choix d'implémentation)
  };

  API.addBookmark(data)
    .then(() => listBookmarks())
    .catch(error => {
      alert(error);
      listBookmarks();
    });
  // on reset les champs
  document.querySelector("#add input[name='title']").value = "";
  document.querySelector("#add input[name='link']").value = "";
  document.querySelector("#add input[name='description']").value = "";
}

/* Handles the click on a bookmark */
async function clickBookmark(bookmark) {
  // si le tag est déjà selected, on ne fait rien
  if (bookmark.classList.contains("selected")) return;

  // On récupère les tags pour les afficher afin de pouvoir en ajouter ou retirer du bookmark
  const tags = (await API.getTags().then(response => response.json())).data;
  if (tags === undefined) {
    alert("Error while getting tags");
    return;
  }
  // if another tag is selected, unselect it
  const selected = document.querySelector("#items div.selected");
  if (selected !== null) {
    // re-display content of selected
    selected.classList.remove("selected");
    selected.querySelector("h2").style.display = "";
    selected.querySelector("a").style.display = "";
    selected.querySelector("div.description").style.display = "";
    selected.querySelector("ul.tags").style.display = "";

    // remove inputs, labels and buttons
    selected.querySelectorAll("input,button,label").forEach(element => {
      element.remove();
    });
    selected.querySelector("br").remove();
    selected.querySelector("span.selection").remove();
  }
  bookmark.classList.add("selected");
  // On remplace le titre par un input pour modifier le titre
  bookmark.querySelector("h2").style.display = "none";
  const inputTitle = document.createElement("input");
  inputTitle.type = "text";
  inputTitle.name = "title";
  inputTitle.value = bookmark.querySelector("h2").textContent;
  inputTitle.placeholder = "Title";
  bookmark.appendChild(inputTitle);
  // On remplace la description par un input pour modifier la description
  bookmark.querySelector("div.description").style.display = "none";
  const inputDescription = document.createElement("input");
  inputDescription.type = "text";
  inputDescription.name = "description";
  inputDescription.value = bookmark.querySelector("div.description").textContent;
  inputDescription.placeholder = "Description";
  bookmark.appendChild(inputDescription);
  // On remplace le lien par un input pour modifier le lien
  bookmark.querySelector("a").style.display = "none";
  const inputLink = document.createElement("input");
  inputLink.type = "text";
  inputLink.name = "link";
  inputLink.value = bookmark.querySelector("a").textContent;
  inputLink.placeholder = "Link";
  bookmark.appendChild(inputLink);

  // add button "modify"
  const modifyButton = document.createElement("button");
  modifyButton.textContent = "Modify";
  modifyButton.type = "button";
  modifyButton.addEventListener("click", modifyBookmark);
  bookmark.appendChild(modifyButton);
  // add button "remove"
  const removeButton = document.createElement("button");
  removeButton.textContent = "Remove";
  removeButton.type = "button";
  removeButton.addEventListener("click", removeBookmark);
  bookmark.appendChild(removeButton);

  // gestion des tags
  bookmark.querySelector("ul.tags").style.display = "none";
  bookmark.appendChild(document.createElement("br"));
  const span = document.createElement("span");
  span.textContent = "Select les tags : ";
  span.classList.add("selection");
  bookmark.appendChild(span);
  tags.forEach(tag => {
    const checkbox = document.createElement("input");
    checkbox.type = "checkbox";
    checkbox.name = "tag";
    checkbox.value = tag.name;
    checkbox.setAttribute("num", tag.id);
    if (bookmark.querySelector(`ul.tags li[num="${tag.id}"]`) !== null)
      checkbox.setAttribute("checked", true);

    bookmark.appendChild(checkbox);
    const label = document.createElement("label");
    label.textContent = tag.name;
    bookmark.appendChild(label);
  });
}

/**
 * Handles the click on the "update" button of a bookmark
 */
function modifyBookmark() {
  const bookmark = document.querySelector("#items div.selected");
  const title = bookmark.querySelector("input[name='title']").value;
  const link = bookmark.querySelector("input[name='link']").value;
  const description = bookmark.querySelector("input[name='description']").value;
  const tags = [];
  // boucle qui récupère les tags cochés
  document.querySelectorAll("#items div.selected input[name='tag']:checked").forEach(checkbox => {
    tags.push({ id: checkbox.getAttribute("num"), name: checkbox.value });
  });
  const data = {
    title: title,
    link: link,
    description: description,
    tags: tags,
  };

  API.updateBookmark(bookmark.getAttribute("num"), data)
    .then(() => listBookmarks())
    .catch(error => {
      alert(error);
      listBookmarks();
    });
}

/**
 * Handles the click on the "remove" button of a bookmark
 */
function removeBookmark() {
  const bookmark = document.querySelector("#items div.selected");
  API.deleteBookmark(bookmark.getAttribute("num"))
    .then(() => listBookmarks())
    .catch(error => {
      alert(error);
      listBookmarks();
    });
}
/* Loads the list of all tags and displays them */
function listTags() {
  document.getElementById("items").innerHTML = ""; // remove all child nodes (plutôt que de faire un for each sur les enfants et removeChild)
  API.getTags()
    .then(response => response.json())
    .then(data => {
      // do a for each over data with index
      data.data.forEach(tag => {
        const newTag = document.querySelector("div.model.tag").cloneNode(true);
        // add h2 containing tag.name
        newTag.querySelector("h2").textContent = tag.name;
        newTag.classList.remove("model");
        newTag.classList.add("item");
        newTag.setAttribute("num", tag.id); // add attribut num tag.id
        document.getElementById("items").appendChild(newTag);
      });
    })
    .catch(error => {
      alert(error);
      throw error;
    });
}

/* Adds a new tag */
function addTag() {
  // verify that the tag name is not empty
  const tagName = document.querySelector("input[name = name]").value;
  if (tagName === null) {
    alert("The tag name cannot be empty");
    throw new Error("The tag name cannot be empty");
  }
  if (tagName === "") {
    alert("The tag name cannot be empty");
    return;
  }
  API.addTag(tagName)
    .then(() => listTags())
    .catch(error => {
      alert(error);
      listTags();
    });

  document.querySelector("input[name = name]").value = "";
}

/* Handles the click on a tag */
function clickTag(tag) {
  // if tag is selected do nothing
  if (tag.classList.contains("selected")) return;
  // if another tag is selected, unselect it
  const selected = document.querySelector("#items div.selected");
  if (selected !== null) {
    selected.classList.remove("selected");
    selected.querySelector("h2").style.display = "";
    selected.querySelectorAll("input,button").forEach(element => {
      element.remove();
    }); // remove inputs and buttons
  }
  tag.classList.add("selected");
  // cacher le h2 de tag
  tag.querySelector("h2").style.display = "none";
  // add text input with tag.name as value
  const input = document.createElement("input");
  input.type = "text";
  input.value = tag.querySelector("h2").textContent;
  input.placeholder = "Title";
  tag.appendChild(input);
  // add button "modify"
  const modifyButton = document.createElement("button");
  modifyButton.textContent = "Modify";
  modifyButton.type = "button";
  modifyButton.addEventListener("click", modifyTag);
  tag.appendChild(modifyButton);
  // add button "remove"

  const removeButton = document.createElement("button");
  removeButton.textContent = "Remove";
  removeButton.type = "button";
  removeButton.addEventListener("click", removeTag);
  tag.appendChild(removeButton);
}

/* Performs the modification of a tag */
function modifyTag() {
  const tag = document.querySelector("#items div.selected");
  const tagName = tag.querySelector("input").value;
  if (tagName === "") {
    alert("The tag name cannot be empty");
    return;
  }
  API.updateTag(tag.getAttribute("num"), tagName)
    .then(() => listTags())
    .catch(error => {
      alert(error);
      listTags();
    });
}

/* Removes a tag */
function removeTag() {
  const tag = document.querySelector("#items div.selected");
  API.deleteTag(tag.getAttribute("num"))
    .then(() => listTags())
    .catch(error => {
      alert(error);
      listTags();
    });
}
/* On document loading */
function miseEnPlace() {
  // Prevent the form from being submitted
  document.querySelector("form").addEventListener("submit", e => e.preventDefault(), false);

  /* Give access token for future ajax requests */
  // Put the name of the current user into <h1>
  setIdentity();
  // Adapt the height of <div id="contents"> to the navigator window
  setContentHeight();
  window.addEventListener("resize", setContentHeight);
  // Listen to the clicks on menu items
  for (let element of document.querySelectorAll("#menu li")) {
    element.addEventListener(
      "click",
      function () {
        const isTags = this.classList.contains("tags");
        selectObjectType(isTags ? "tags" : "bookmarks");
      },
      false
    );
  }
  // Initialize the object type to "tags"
  selectObjectType("tags");
  // Listen to clicks on the "add tag" button

  document.getElementById("addTag").addEventListener("click", addTag, false);
  document.getElementById("addBookmark").addEventListener("click", addBookmark, false);
  document.getElementById("items").addEventListener(
    "click",
    e => {
      // Listen to clicks on the tag items
      const tag = e.target.closest(".tag.item");
      if (tag !== null) {
        clickTag(tag);
        return;
      }
      // Questions 10 & 12 - Listen to clicks on bookmark items
      const bookmark = e.target.closest(".bookmark.item");
      if (bookmark !== null) {
        clickBookmark(bookmark);
      }
    },
    false
  );
}
window.addEventListener("load", miseEnPlace, false);
