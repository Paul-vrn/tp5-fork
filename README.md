
# TP5Backend

## Informations

Lien frontend : [https://tp5backendpaulv.osc-fr1.scalingo.io/frontend/](https://tp5backendpaulv.osc-fr1.scalingo.io/frontend/)

## Cliquez sur les badges

J'ai la flemme de faire les tests de bookmarks donc le coverage est pas ouf, c'est pour l'idée.\
Cliquez sur les badges pour avoir les reports.

[![Branches](https://grenoble-inp-ensimag.gitlab.io/4MMCAW6/G4/TP5_Backend/TP5Backend_paul_vernin/badge-branches.svg)](https://grenoble-inp-ensimag.gitlab.io/4MMCAW6/G4/TP5_Backend/TP5Backend_paul_vernin/lcov-report/)
[![Functions](https://grenoble-inp-ensimag.gitlab.io/4MMCAW6/G4/TP5_Backend/TP5Backend_paul_vernin/badge-functions.svg)](https://grenoble-inp-ensimag.gitlab.io/4MMCAW6/G4/TP5_Backend/TP5Backend_paul_vernin/lcov-report/)
[![Lines](https://grenoble-inp-ensimag.gitlab.io/4MMCAW6/G4/TP5_Backend/TP5Backend_paul_vernin/badge-lines.svg)](https://grenoble-inp-ensimag.gitlab.io/4MMCAW6/G4/TP5_Backend/TP5Backend_paul_vernin/lcov-report/)
[![Statements](https://grenoble-inp-ensimag.gitlab.io/4MMCAW6/G4/TP5_Backend/TP5Backend_paul_vernin/badge-statements.svg)](https://grenoble-inp-ensimag.gitlab.io/4MMCAW6/G4/TP5_Backend/TP5Backend_paul_vernin/lcov-report/)
[![Eslint](https://grenoble-inp-ensimag.gitlab.io/4MMCAW6/G4/TP5_Backend/TP5Backend_paul_vernin/badge-eslint.svg)](https://grenoble-inp-ensimag.gitlab.io/4MMCAW6/G4/TP5_Backend/TP5Backend_paul_vernin/report.html)